## StarWars - Anthony Collins (AC) ##

Application to help my friend Anthony Collins (AC) to process some information from StarWars.


### Technologies ###

* JDK 8
* Tomcat 8 
* JEE 6
* Jersey
* DB Derby
* Log4J
* JUnit

### How do I get set up? ###

* You nedd to clone this repo and import maven project in eclipse.
* After, you run maven with goals clean install
* Run maven with goal tomcat:run
* At the first time make a request to create database


### EndPoints ###

#### CREATE DATABASE ####

URL `http://localhost:8080/RESTfulAVProject/rest/database/create`

#### POST /script ####

URL `http://localhost:8080/RESTfulAVProject/rest/receive/script`

Responses

● 200: Success message
`{ "message": "Movie script successfully received" }`

● 403: Forbidden message
`{ "message": "Movie script already received" }`


#### GET /settings - GET /settings/{id} ####

URL `http://localhost:8080/RESTfulAVProject/rest/settings/{id}`

Responses

● 200: Success message

● 403: Forbidden message


#### GET /characters - GET /characters/{id} ####

URL `http://localhost:8080/RESTfulAVProject/rest/characters/{id}`

Responses

● 200: Success message

● 403: Forbidden message