package com.av.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.av.vo.SettingCharacterVO;

public class SettingCharacterDAO {

	public void insert(Integer idSetting, Integer idCharacter) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			SettingCharacterVO settingCharacter = this.find(idSetting, idCharacter);
			if(settingCharacter == null){
				String query = "insert into tb_setting_character (id_setting, id_character) values (?, ?)";
						
				conn = BasicDAO.getConnectionToDerby();
	
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, idSetting);
				stmt.setInt(2, idCharacter);
				
				stmt.executeUpdate();
			}			
		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
	}
	
	public SettingCharacterVO find(Integer idSetting, Integer idCharacter) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		SettingCharacterVO settingCharacter = null;
		
		try {
			String query = "select * from tb_setting_character where id_setting = ?  and id_character = ?";
					
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			stmt.setInt(1, idSetting);
			stmt.setInt(2, idCharacter);
			
			
			ResultSet rs = stmt.executeQuery();
			
			
			while(rs.next()){
				settingCharacter = new SettingCharacterVO();
				settingCharacter.setId(rs.getInt("id"));
				settingCharacter.setIdSetting(rs.getInt("id_setting"));
				settingCharacter.setIdCharacter(rs.getInt("id_character"));
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return settingCharacter;
	}
}
