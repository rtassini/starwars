package com.av.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.av.vo.CharacterVO;
import com.av.vo.WordVO;

public class WordDAO {

	public Integer insert(String word, Integer idCharacter) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			
				String query = "insert into tb_word (word, id_character) values (?, ?)";
						
				conn = BasicDAO.getConnectionToDerby();
	
				stmt = conn.prepareStatement(query);
				stmt.setString(1, word);
				stmt.setInt(2, idCharacter);
				
				stmt.executeUpdate();
						
		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return idCharacter;
	}
	
	public List<Map<String, Object>> find(Integer idCharacter) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		Map<String, Object> mapVO = null;
		List<Map<String, Object>> list = null;
		
		try {
			String query = " select count(word) count, word from tb_word where id_character = ? group by word ";
			
			
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			stmt.setInt(1, idCharacter);
			
			
			ResultSet rs = stmt.executeQuery();
			
			list = new ArrayList<Map<String, Object>>();
			while(rs.next()){
				mapVO = new HashMap<String, Object>();
				mapVO.put("word", rs.getString("word"));
				mapVO.put("count", rs.getInt("count"));
				
				list.add(mapVO);
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return list;
	}
}
