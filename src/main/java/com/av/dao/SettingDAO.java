package com.av.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.av.vo.SettingVO;


public class SettingDAO {

	public Integer insert(String name) throws SQLException {
		Integer idSetting = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			
			SettingVO setting = this.find(name);
			if(setting == null){
				
				String query = "insert into tb_setting (name) values (?)";
						
				conn = BasicDAO.getConnectionToDerby();
	
				stmt = conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
				stmt.setString(1, name);
				
				
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();

				while (rs.next()) {
					idSetting = rs.getInt(1);
				}
			}else {
				idSetting = setting.getId();
			}

			
		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}

		return idSetting;
	}
	
	
	public SettingVO find(String name) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		SettingVO setting = null;
		
		try {
			String query = "select * from tb_setting where name = ? ";
					
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			stmt.setString(1, name);
			
			
			ResultSet rs = stmt.executeQuery();
			
			
			while(rs.next()){
				setting = new SettingVO();
				setting.setId(rs.getInt("id"));
				setting.setName(rs.getString("name"));
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return setting;
	}
	
	public List<SettingVO> find(Integer id) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		SettingVO setting = null;
		List<SettingVO> list = null;
		
		try {
			String query = "select * from tb_setting ";
			if (id != null)
				query += " where id = ? ";
			
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			if (id != null)
				stmt.setInt(1, id);
			
			
			ResultSet rs = stmt.executeQuery();
			
			list = new ArrayList<SettingVO>();
			while(rs.next()){
				setting = new SettingVO();
				setting.setId(rs.getInt("id"));
				setting.setName(rs.getString("name"));
				
				list.add(setting);
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return list;
	}
}
