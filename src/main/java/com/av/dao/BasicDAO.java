package com.av.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class BasicDAO {

	static final Logger logger = Logger.getLogger(BasicDAO.class);
	
	public static Connection getConnectionToDerby() {
		// -------------------------------------------
		// URL format is
		// jdbc:derby:<local directory to save data>
		// -------------------------------------------
		String dbUrl = "jdbc:derby:c:\\home\\DB\\StarWars;create=true";
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(dbUrl);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
	public static void createTbFile() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			conn = BasicDAO.getConnectionToDerby();
			stmt = conn.createStatement();

			StringBuilder createTableFile = new StringBuilder("CREATE TABLE tb_file (id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), name VARCHAR(256) NOT NULL, PRIMARY KEY (id)) " );
			
			// create table
			stmt.executeUpdate(createTableFile.toString());
			
			logger.info("create table tb_file");
		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
	}
	
	public static void createTbSetting() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			conn = BasicDAO.getConnectionToDerby();
			stmt = conn.createStatement();

			StringBuilder createTableSetting = new StringBuilder(" CREATE TABLE tb_setting (id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), name VARCHAR(256) NOT NULL, PRIMARY KEY (id)) ");
		
			// create table
			stmt.executeUpdate(createTableSetting.toString());
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
	}
	
	public static void createTbCharacter() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			conn = BasicDAO.getConnectionToDerby();
			stmt = conn.createStatement();

			StringBuilder createTableCharacter = new StringBuilder("CREATE TABLE tb_character (id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), name VARCHAR(256) NOT NULL, PRIMARY KEY (id)) " );
			
			// create table
			stmt.executeUpdate(createTableCharacter.toString());
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
	}
	
	public static void createTbSettingCharacter() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			conn = BasicDAO.getConnectionToDerby();
			stmt = conn.createStatement();

			StringBuilder createTableSettingCharacter = new StringBuilder("CREATE TABLE tb_setting_character (id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), id_setting INT NOT NULL, id_character INT NOT NULL, PRIMARY KEY (id), CONSTRAINT SETTING_TB FOREIGN KEY (id_setting) REFERENCES tb_setting(id), CONSTRAINT CHARACTER_TB FOREIGN KEY (id_character) REFERENCES tb_character(id)) " );
			
			// create table
			stmt.executeUpdate(createTableSettingCharacter.toString());
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
	}
	
	public static void createTbWord() throws SQLException {
		Connection conn = null;
		Statement stmt = null;
		
		try {
			conn = BasicDAO.getConnectionToDerby();
			stmt = conn.createStatement();

			StringBuilder createTableWord = new StringBuilder("CREATE TABLE tb_word (id INT NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), word VARCHAR(256) NOT NULL, id_character INT NOT NULL,PRIMARY KEY (id), CONSTRAINT CHARACTER_WORD FOREIGN KEY (id_character)REFERENCES tb_character (id)) " );
			
			// create table
			stmt.executeUpdate(createTableWord.toString());
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
	}
	
	public static void createDB() throws SQLException {
		createTbFile();
		createTbSetting();
		createTbCharacter();
		createTbSettingCharacter();
		createTbWord();
	}
	
	public static void dropTable() throws SQLException {
		Connection conn = BasicDAO.getConnectionToDerby();
		Statement stmt = conn.createStatement();
		
		stmt.executeUpdate("Drop Table setting");
	}
	
	
	public static void closeObjects(ResultSet rs, Statement stmt, Connection conn) throws SQLException{
		if(rs != null) rs.close();
		if(stmt != null) stmt.close();
		if(conn != null) conn.close();
	}
		
}
