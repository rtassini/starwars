package com.av.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.av.vo.FileVO;


public class FileDAO {

	public void insert(String name) throws Exception {
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			
			FileVO fileVO = this.find(name);
			if(fileVO == null){
				
				String query = "insert into tb_file (name) values (?)";
						
				conn = BasicDAO.getConnectionToDerby();
	
				stmt = conn.prepareStatement(query);
				stmt.setString(1, name);
				
				
				stmt.executeUpdate();
			} else {
				throw new Exception("Movie script already received");
			}
		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}

	}
	
	
	public FileVO find(String name) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		FileVO fileVO = null;
		
		try {
			String query = "select * from tb_file where name = ? ";
					
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			stmt.setString(1, name);
			
			
			ResultSet rs = stmt.executeQuery();
			
			
			while(rs.next()){
				fileVO = new FileVO();
				fileVO.setId(rs.getInt("id"));
				fileVO.setName(rs.getString("name"));
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return fileVO;
	}
}
