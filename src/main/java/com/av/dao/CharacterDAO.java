package com.av.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.av.vo.CharacterVO;
import com.av.vo.SettingVO;

public class CharacterDAO {

	public Integer insert(String name) throws SQLException {
		Integer idCharacter = null;
		Connection conn = null;
		PreparedStatement stmt = null;
		try {
			
			CharacterVO characterVO = this.find(name);
			if(characterVO == null){
				
				String query = "insert into tb_character (name) values (?)";
						
				conn = BasicDAO.getConnectionToDerby();
	
				stmt = conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
				stmt.setString(1, name);
				
				stmt.executeUpdate();
				
				ResultSet rs = stmt.getGeneratedKeys();

				while (rs.next()) {
					idCharacter = rs.getInt(1);
				}
			}else {
				idCharacter = characterVO.getId();
			}
		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return idCharacter;
	}
	
	
	public CharacterVO find(String name) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		CharacterVO characterVO = null;
		
		try {
			String query = "select * from tb_character where name = ? ";
					
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			stmt.setString(1, name);
			
			
			ResultSet rs = stmt.executeQuery();
			
			
			while(rs.next()){
				characterVO = new CharacterVO();
				characterVO.setId(rs.getInt("id"));
				characterVO.setName(rs.getString("name"));
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return characterVO;
	}
	
	public List<CharacterVO> findByIdSetting(Integer idSetting) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		CharacterVO character = null;
		List<CharacterVO> list = null;
		
		try {
			String query = " select " 
				+ "	tb_character.* " 
				+ " from tb_setting_character "
				+ " inner join tb_character on tb_setting_character.id_character = tb_character.id " 
				+ " where tb_setting_character.id_setting = ? ";
			
			
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			stmt.setInt(1, idSetting);
			
			
			ResultSet rs = stmt.executeQuery();
			
			list = new ArrayList<CharacterVO>();
			while(rs.next()){
				character = new CharacterVO();
				character.setId(rs.getInt("id"));
				character.setName(rs.getString("name"));
				
				list.add(character);
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return list;
	}
	
	public List<CharacterVO> find(Integer idCharacter) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		CharacterVO character = null;
		List<CharacterVO> list = null;
		
		try {
			String query = " select * from tb_character "; 
			if (idCharacter != null)
				query += " where id = ? ";
			
			conn = BasicDAO.getConnectionToDerby();

			stmt = conn.prepareStatement(query);
			if (idCharacter != null)
				stmt.setInt(1, idCharacter);
			
			
			ResultSet rs = stmt.executeQuery();
			
			list = new ArrayList<CharacterVO>();
			while(rs.next()){
				character = new CharacterVO();
				character.setId(rs.getInt("id"));
				character.setName(rs.getString("name"));
				
				list.add(character);
			}
			

		} finally{
			BasicDAO.closeObjects(null, stmt, conn);
		}
		
		return list;
	}
}
