package com.av.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.av.controller.CharacterController;
import com.av.dao.BasicDAO;


@Path("/characters")
public class CharacterView {
	
	static final Logger logger = Logger.getLogger(BasicDAO.class);
	
	@SuppressWarnings("finally")
	@GET
	@Path("/{param}")
	public Response getSettings(@PathParam("param") String id) {
 
		logger.info("Get settings");
		CharacterController controller = new CharacterController(); 
		JSONObject json = new JSONObject();
		Status status =  Response.Status.OK;
		Integer idCharacter = Integer.parseInt(id);
		try {
			json.put("message", controller.getCharacters(idCharacter));
		
		
		}catch (Exception e){
			json.put("message", e.getMessage());
			status =  Response.Status.FORBIDDEN;
			
		} finally{
			return Response.status(status).entity(json.toString()).build();
		}
 
 
	}
	
	
	@SuppressWarnings("finally")
	@GET
	public Response getSettings() {
 
		CharacterController controller = new CharacterController(); 
		JSONObject json = new JSONObject();
		Status status =  Response.Status.OK;
		Integer idCharacter = null;
		try {
			json.put("message", controller.getCharacters(idCharacter));
		
		
		}catch (Exception e){
			json.put("message", e.getMessage());
			status =  Response.Status.FORBIDDEN;
			
		} finally{
			return Response.status(status).entity(json.toString()).build();
		}
 
	}

	

}
