package com.av.rest;

import java.sql.SQLException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.av.dao.BasicDAO;


@Path("/database")
public class DataBase {
	
	@GET
	@Path("/create")
	public Response getMsg() {
 
		try {
			BasicDAO.createDB();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 
		return Response.status(200).build();
 
	}
 
}
