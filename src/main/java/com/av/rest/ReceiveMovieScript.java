package com.av.rest;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;

import com.av.controller.ReceiveMovieScriptController;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;


@Path("/receive")
public class ReceiveMovieScript {
	
	
	@SuppressWarnings("finally")
	@POST
	@Path("/script")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
			@FormDataParam("file") InputStream uploadedInputStream,
		@FormDataParam("file") FormDataContentDisposition fileDetail) {
		
		
		ReceiveMovieScriptController controller = new ReceiveMovieScriptController(); 
		
		JSONObject json = new JSONObject();
		Status status =  Response.Status.OK;
		
		try {
			controller.insertFile(fileDetail.getFileName());
			
			controller.loadFile(uploadedInputStream);
	
			json.put("message", "Movie script successfully received");
		
		
		}catch (Exception e){
			json.put("message", e.getMessage());
			status =  Response.Status.FORBIDDEN;
			
		} finally{
			return Response.status(status).entity(json.toString()).build();
		}
		

	}
	
	
	
	

}
