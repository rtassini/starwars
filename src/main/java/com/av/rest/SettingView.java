package com.av.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.json.JSONObject;

import com.av.controller.SettingController;


@Path("/settings")
public class SettingView {
	
	
	@SuppressWarnings("finally")
	@GET
	@Path("/{param}")
	public Response getSettings(@PathParam("param") String id) {
 
		SettingController controller = new SettingController(); 
		JSONObject json = new JSONObject();
		Status status =  Response.Status.OK;
		Integer idSettings = Integer.parseInt(id);
		try {
			json.put("message", controller.getSettings(idSettings));
		
		
		}catch (Exception e){
			json.put("message", e.getMessage());
			status =  Response.Status.FORBIDDEN;
			
		} finally{
			return Response.status(status).entity(json.toString()).build();
		}
 
 
	}
	
	
	@SuppressWarnings("finally")
	@GET
	public Response getSettings() {
 
		SettingController controller = new SettingController(); 
		JSONObject json = new JSONObject();
		Status status =  Response.Status.OK;
		Integer idSettings = null;
		try {
			json.put("message", controller.getSettings(idSettings));
		
		
		}catch (Exception e){
			json.put("message", e.getMessage());
			status =  Response.Status.FORBIDDEN;
			
		} finally{
			return Response.status(status).entity(json.toString()).build();
		}
 
	}

	

}
