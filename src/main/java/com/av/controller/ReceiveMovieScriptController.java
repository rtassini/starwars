package com.av.controller;

import java.io.InputStream;
import java.util.Scanner;

import com.av.dao.CharacterDAO;
import com.av.dao.FileDAO;
import com.av.dao.SettingCharacterDAO;
import com.av.dao.SettingDAO;
import com.av.dao.WordDAO;

public class ReceiveMovieScriptController {

	
	public static final String EXT = "EXT. ";
	public static final String INT = "INT. ";
	public static final String INT_EXT = "INT./EXT. ";
	public static final String SEPARATOR = " - ";
	public static final int INDENTED_CHARACTER = 22;
	public static final int INDENTED_DIALOGUE = 10;
	
	public void loadFile(InputStream uploadedInputStream){
		Scanner scanner = null;
		SettingDAO settingDAO = null;
		SettingCharacterDAO settingCharacterDAO = null;
		CharacterDAO characterDAO = null;
		WordDAO wordDAO = null;
		Integer idSetting = null, idCharacter = null;
		
		try {
			scanner = new Scanner(uploadedInputStream)
					.useDelimiter("\\n");
		
			settingDAO = new SettingDAO();
			settingCharacterDAO = new SettingCharacterDAO();
			characterDAO = new CharacterDAO();
			wordDAO = new WordDAO();
			while (scanner.hasNext()) {
				String line = scanner.next();
				
				if(line.toUpperCase().equals(line)
						&& (line.startsWith(EXT) || line.startsWith(INT) || line.startsWith(INT_EXT) )){
					
					String [] settings = line.replace(EXT, "")
							.replace(INT, "")
							.replace(INT_EXT, "")
							.split(SEPARATOR);
					
					idSetting = settingDAO.insert((settings[0].trim()));
				} else {
				
					int indented = 0;
					for (char charecter : line.toCharArray()) {
						if(Character.isWhitespace(charecter)) indented++;
						else break;
					}
					
					if(line.toUpperCase().equals(line)
							&& indented == INDENTED_CHARACTER){
						idCharacter = characterDAO.insert(line.trim());
						settingCharacterDAO.insert(idSetting, idCharacter);
					}else if(indented == INDENTED_DIALOGUE){
						String [] words = line.trim()
								.replace( "." , "")
								.replace( "!" , "")
								.replace( "?" , "")
								.replace( "," , "")
								.split(" ");
						for (String word : words) {
							wordDAO.insert(word, idCharacter);
						}
						
					}
								
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			scanner.close();
		}
	}
	
	
	public void insertFile(String fileName) throws Exception{
		FileDAO dao = new FileDAO();
		dao = new FileDAO();
		dao.insert(fileName);
	}
	
}
