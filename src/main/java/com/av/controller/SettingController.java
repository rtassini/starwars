package com.av.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.av.dao.CharacterDAO;
import com.av.dao.SettingDAO;
import com.av.dao.WordDAO;
import com.av.vo.CharacterVO;
import com.av.vo.SettingVO;

public class SettingController {

	
	public List<Map<String, Object>> getSettings(Integer idSettings) throws SQLException{
		List<Map<String, Object>> listReturn = new ArrayList<Map<String, Object>>();
		SettingDAO dao = new SettingDAO();
		CharacterDAO characterDAO = new CharacterDAO();
		WordDAO wordDAO = new WordDAO();
		
		List<SettingVO> listSetting = dao.find(idSettings);
		
		for (SettingVO settingVO : listSetting) {
			Map<String, Object> mapReturn = new HashMap<String, Object>();
			mapReturn.put("name", settingVO.getName());
			mapReturn.put("id", settingVO.getId());
			
			List<CharacterVO> listCharacter = characterDAO.findByIdSetting(settingVO.getId());
			
			List<Map<String, Object>> listCharacterFinal = new ArrayList<Map<String, Object>>();
			for (CharacterVO characterVO : listCharacter) {
				Map<String, Object> mapCharacter = new HashMap<String, Object>();
				mapCharacter.put("name", characterVO.getName());
				mapCharacter.put("id", characterVO.getId());
				
				List<Map<String, Object>> listWord = wordDAO.find(characterVO.getId());
				mapCharacter.put("wordCounts", listWord);
				
				listCharacterFinal.add(mapCharacter);
			}
			
			
			mapReturn.put("character", listCharacterFinal);
			
			
			listReturn.add(mapReturn);
		}
		
		return listReturn;
		
	}
}
