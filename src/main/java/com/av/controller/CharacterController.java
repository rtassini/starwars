package com.av.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.av.dao.CharacterDAO;
import com.av.dao.WordDAO;
import com.av.vo.CharacterVO;

public class CharacterController {

	public List<Map<String, Object>> getCharacters(Integer idCharacter)
			throws SQLException {
		List<Map<String, Object>> listReturn = new ArrayList<Map<String, Object>>();
		CharacterDAO characterDAO = new CharacterDAO();
		WordDAO wordDAO = new WordDAO();

		Map<String, Object> mapReturn = new HashMap<String, Object>();

		List<CharacterVO> listCharacter = characterDAO.find(idCharacter);

		List<Map<String, Object>> listCharacterFinal = new ArrayList<Map<String, Object>>();
		for (CharacterVO characterVO : listCharacter) {
			Map<String, Object> mapCharacter = new HashMap<String, Object>();
			mapCharacter.put("name", characterVO.getName());
			mapCharacter.put("id", characterVO.getId());

			List<Map<String, Object>> listWord = wordDAO.find(characterVO
					.getId());
			mapCharacter.put("wordCounts", listWord);

			listCharacterFinal.add(mapCharacter);
		}

		mapReturn.put("character", listCharacterFinal);

		listReturn.add(mapReturn);

		return listReturn;

	}
}
