package com.av.vo;

public class SettingCharacterVO {

	private Integer id;
	private Integer idSetting;
	private Integer idCharacter;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getIdSetting() {
		return idSetting;
	}
	public void setIdSetting(Integer idSetting) {
		this.idSetting = idSetting;
	}
	public Integer getIdCharacter() {
		return idCharacter;
	}
	public void setIdCharacter(Integer idCharacter) {
		this.idCharacter = idCharacter;
	}
	
}
